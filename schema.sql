CREATE TABLE public.request_history
(
    id SERIAL PRIMARY KEY NOT NULL,
    path VARCHAR(1000) NOT NULL,
    headers TEXT NOT NULL,
    method VARCHAR(4) NOT NULL,
    body TEXT NOT NULL,
    version VARCHAR(10) NOT NULL,
    ip VARCHAR(15) NOT NULL,
    requested_at TIMESTAMP WITH TIME ZONE DEFAULT NOW() NOT NULL
);

COMMENT ON COLUMN public.request_history.path IS 'Длина ulr стандартами не регламентирована, допусти что она не будет превышать 1000';