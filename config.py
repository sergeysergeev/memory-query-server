import yaml
import random

class MyConfig:
    __instance = None
    config = None
    def __init__(self):
        if MyConfig.__instance != None :
            raise Exception("You shall not pass")
        else:
            with open("config.yml", 'r') as ymlfile:
                MyConfig.config = yaml.load(ymlfile)
        MyConfig.__instance = self
    @staticmethod
    def getInstance():
        if MyConfig.__instance == None:
            MyConfig()
        return MyConfig.__instance
    @staticmethod
    def getDbConfig():
        conf = MyConfig.getInstance().config['postgres']
        return (conf['dbname'], conf['user'], conf['password'], conf['host'], conf['port'])
    @staticmethod
    def getAppHost():
        return MyConfig.getInstance().config['application']['host']
    @staticmethod
    def getAppPort():
        return MyConfig.getInstance().config['application']['port']
    @staticmethod
    def setMode(ind):
        MyConfig.getInstance().config['application']['mode'] = ind
    @staticmethod
    def getMode():
        return MyConfig.getInstance().config['application']['mode']
    @staticmethod
    def getErrors():
        return MyConfig.getInstance().config['application']['errors']
    @staticmethod
    def getRandonError():
        errorList = MyConfig.getErrors()
        if len(errorList) > 0:
            return errorList[random.randint(0,len(errorList)-1)]
        return 0
