Server supports only GET and POST methods.

For changing server mode pls send get parameter mode with valuest _fist_ or _second_ for 
* first mode - server sends responses with code 200 success and insert row about request to DB;
* second mode - server sends responses with random 4xx or 5xx http code without insert request to DB;

To show history send request with parameter history=show.

Starting:
~~~~
docker-compose up -d
python main.py
~~~~