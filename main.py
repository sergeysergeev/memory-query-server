import time
import BaseHTTPServer
from StringIO import StringIO

import psycopg2
import urlparse
import json

from config import MyConfig


class MyHandler(BaseHTTPServer.BaseHTTPRequestHandler):
    def do_POST(s):
        s.common_process()
    def do_GET(s):
        s.common_process()
    def common_process(s):
        global WORK_MODE
        global cur
        if not s.settingPreprocess():
            return
        if MyConfig.getMode() == 1:
            s.addRequest()
        else:
            s.showError()
    def addRequest(s):
        sqlRequest = "INSERT INTO public.request_history ( path, headers, method, body, version, ip) VALUES (%s,%s,%s,%s,%s,%s);"
        content_len = int(s.headers.getheader('content-length', 0))
        body = s.rfile.read(content_len)
        clientHost, clientPort = s.client_address
        args = s.path, s.headers.__str__(), s.command, body, s.protocol_version, clientHost
        cur.execute(sqlRequest, args)
        conn.commit()
        s.send_response(200)
        s.setResponseJsonBody({'status':'success'})
    def showError(s):
        randomError = MyConfig.getRandonError()
        if randomError == 0:
            s.send_response(500)
        else:
            s.send_response(randomError)
        s.setResponseJsonBody({'status':'fail'})
    def showHistory(s):
        s.send_response(200)
        cur.execute("SELECT path, headers, method, body, version, ip, requested_at  from public.request_history")
        history = []
        for row in cur.fetchall():
            path, headers, method, body, version, ip, date = row
            history.append({'path': path, 'method': method, 'headers': headers, 'body': body, 'version': version, 'ip': ip, 'date': date.isoformat()})
        s.send_header("Content-type", "application/json")
        s.end_headers()
        io = StringIO()
        json.dump(history, io)
        s.wfile.write(io.getvalue())

    def setResponseJsonBody(s, mes):
        s.send_header("Content-type", "application/json")
        s.end_headers()
        io = StringIO()
        json.dump(mes, io)
        s.wfile.write(io.getvalue())

    def settingPreprocess(s):
        parsed_path = urlparse.urlparse(s.path)
        try:
            params = dict([p.split('=') for p in parsed_path[4].split('&')])
        except:
            params = {}
        if params.has_key('mode'):
            if params['mode'] == 'first':
                MyConfig.setMode(1)
            if params['mode'] == 'second':
                MyConfig.setMode(2)
        if params.has_key('history') and params['history'] == "show":
            s.showHistory()
            return 0
        return 1



if __name__ == '__main__':
    server_class = BaseHTTPServer.HTTPServer
    httpd = server_class((MyConfig.getAppHost(), MyConfig.getAppPort()), MyHandler)
    conn = psycopg2.connect("dbname=%s user=%s password=%s host=%s port=%s" % MyConfig.getDbConfig())
    cur = conn.cursor()
    print time.asctime(), "Server Starts - %s:%s" % (MyConfig.getAppHost(), MyConfig.getAppPort())
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()
    cur.close()
    conn.close()
    print time.asctime(), "Server Stops - %s:%s" % (MyConfig.getAppHost(), MyConfig.getAppPort())